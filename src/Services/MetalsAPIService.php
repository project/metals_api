<?php

namespace Drupal\metals_api\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Cache Controller.
 */
class MetalsAPIService {

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The cache.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Drupal's cache backend.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Drupal's logger service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Drupal's config factory service.
   */
  public function __construct(CacheBackendInterface $cacheBackend, LoggerChannelFactoryInterface $logger, ConfigFactory $configFactory) {
    $this->cacheBackend = $cacheBackend;
    $this->logger = $logger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('logger.factory'),
      $container->get('config.factory')
    );
  }

  /**
   * Loads the metals feed JSON from cache or API.
   *
   * @return array
   *   Returns an array with data and method cached/API.
   */
  public function loadFeed() {
    if ($cache = $this->cacheBackend->get('cached_metals_api_exchangeRates')) {
      return [
        'data' => $cache->data,
        'means' => 'cache',
      ];
    }
    else {
      // Set API Endpoint and API key.
      $endpoint = 'latest';
      $configFactory = $this->configFactory->get('metals_api.settings');
      $metals_api_access_key = $configFactory->get('metals_api_access_key');
      $metals_api_refresh_rate = $configFactory->get('metals_api_refresh_rate');

      // Initialize CURL:
      $ch = curl_init('https://metals-api.com/api/' . $endpoint . '?access_key=' . $metals_api_access_key . '');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      // Store the data:
      $json = curl_exec($ch);
      curl_close($ch);

      // Decode JSON response:
      $exchangeRates = json_decode($json, TRUE);

      $tags = [
        'metals_api',
      ];

      // Cache the response.
      $refresh_rate = "+" . $metals_api_refresh_rate . " hours";
      $this->logger->get('metals_api')->notice("The Metals API Refresh rate is: " . $refresh_rate);
      $this->logger->get('metals_api')->notice("The Metals API Refresh timestamp is: " . (new \DateTime($refresh_rate))->getTimestamp());
      $this->cacheBackend->set('cached_metals_api_exchangeRates', $exchangeRates, (new \DateTime($refresh_rate))->getTimestamp(), $tags);

      return [
        'data' => $exchangeRates,
        'means' => 'API',
      ];
    }
  }

  /**
   * Clears the posts from the cache.
   */
  public function clearPosts() {
    if (!empty($this->cacheBackend->get('cached_metals_api_exchangeRates'))) {
      $this->cacheBackend->delete('cached_metals_api_exchangeRates');
      MessengerInterface::addMessage('Cached precious metal rates have been removed from cache.', 'status');
    }
    else {
      MessengerInterface::addMessage('No precious metal rates in cache.', 'error');
    }
  }

}
