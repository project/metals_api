<?php

namespace Drupal\metals_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\metals_api\Services\MetalsAPIService;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Produce block with Metals Feed acquired from custom MetalsAPIService.
 *
 * @Block(
 * id = "metals_api_block",
 * admin_label = @Translation("Metals API Block"),
 * )
 */
class MetalsAPIBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   Configuration array.
   * @param string $plugin_id
   *   Plugin id variable.
   * @param mixed $plugin_definition
   *   Plugin definition variable.
   * @param \Drupal\metals_api\Services\MetalsAPIService $metalsAPIService
   *   Custom Service consumes and caches Metals API Data.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Drupal's config factory service..
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MetalsAPIService $metalsAPIService, ConfigFactory $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->metals_api_service = $metalsAPIService;
    $this->metals_api_range = [
      "XAG" => "Silver (Troy Ounce)",
      "XAU" => "Gold (Troy Ounce)",
      "XPD" => "Palladium",
      "XPT" => "Platinum",
    ];
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Dependency Injection container interface variable.
   * @param array $configuration
   *   Configuration array.
   * @param string $plugin_id
   *   Plugin id variable.
   * @param mixed $plugin_definition
   *   Plugin definition variable.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('metals_api.feed'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['metals_api_elements_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Metals API Elements'),
      '#description' => $this->t('Select the metals that you would like to render prices for.'),
      '#tree' => TRUE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['metals_api_elements_fieldset']['metals_api_elements'] = [
      '#type' => 'checkboxes',
      '#options' => $this->metals_api_range,
      '#default_value' => isset($config['metals_api_elements_fieldset']['metals_api_elements']) ? $config['metals_api_elements_fieldset']['metals_api_elements'] : [],
    ];

    $form['metals_api_options_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Metals API Options'),
      '#description' => '',
      '#tree' => TRUE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['metals_api_options_fieldset']['metals_api_display_measurement_units'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Units of Measurement?'),
      '#description' => $this->t('Do you wish to display the Display Units of Measurement for this Precious Metals Feed block?'),
      '#default_value' => isset($config['metals_api_options_fieldset']['metals_api_display_measurement_units']) ? $config['metals_api_options_fieldset']['metals_api_display_measurement_units'] : TRUE,
    ];

    $form['metals_api_options_fieldset']['metals_api_display_currency'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Currency?'),
      '#description' => $this->t('Do you wish to display the base currency of this Precious Metals Feed block?'),
      '#default_value' => isset($config['metals_api_options_fieldset']['metals_api_display_currency']) ? $config['metals_api_options_fieldset']['metals_api_display_currency'] : TRUE,
    ];

    $form['metals_api_options_fieldset']['metals_api_display_timestamp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Timestamp?'),
      '#description' => $this->t('Do you wish to display the timestamp of the latest Precious Metals Feed update?'),
      '#default_value' => isset($config['metals_api_options_fieldset']['metals_api_display_timestamp']) ? $config['metals_api_options_fieldset']['metals_api_display_timestamp'] : TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['metals_api_elements_fieldset']['metals_api_elements'] = $values['metals_api_elements_fieldset']['metals_api_elements'];
    $this->configuration['metals_api_options_fieldset']['metals_api_display_measurement_units'] = $values['metals_api_options_fieldset']['metals_api_display_measurement_units'];
    $this->configuration['metals_api_options_fieldset']['metals_api_display_currency'] = $values['metals_api_options_fieldset']['metals_api_display_currency'];
    $this->configuration['metals_api_options_fieldset']['metals_api_display_timestamp'] = $values['metals_api_options_fieldset']['metals_api_display_timestamp'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Acquire the Cached Feed from the MetalsAPIService.
    $metals_api_feed = $this->metals_api_service->loadFeed();
    $dateoffeed = date('M d, Y g:h a (e)', $metals_api_feed['data']['timestamp']);
    $currency = $metals_api_feed['data']['base'];
    $unit = $metals_api_feed['data']['unit'];

    // Acquire the $selection_array to get the lables as well as the symbols.
    // Loop against the $config_array to only pull values that have been set.
    $config = $this->getConfiguration();
    $selection_array = $this->metals_api_range;
    $config_array = $config['metals_api_elements_fieldset']['metals_api_elements'];
    $options_array = [
      'display_measurement_units' => $config['metals_api_options_fieldset']['metals_api_display_measurement_units'],
      'display_currency' => $config['metals_api_options_fieldset']['metals_api_display_currency'],
      'display_timestamp' => $config['metals_api_options_fieldset']['metals_api_display_timestamp'],
    ];

    $rates_array = [];
    $config_array_key = '';
    $metals_feed_item_rate = 0;

    foreach ($selection_array as $key => $item) {
      if ($config_array[$key] != '0') {
        $config_array_key = $config_array[$key];
        $rates_array[$config_array_key]['detail'] = $item;
        // Metals Feed API is now using 1/value for precious metals.
        $metals_feed_item_rate = 1 / ($metals_api_feed['data']['rates'][$config_array_key]);
        $rates_array[$config_array_key]['rate'] = '$' . number_format($metals_feed_item_rate, 2, '.', ',');
      }
    }

    // Return array.
    return [
      '#theme' => 'metals_api_block',
      '#markup' => 'Metals API Block',
      '#metals_api_timestamp' => $this->t('Updated @timestamp', ['@timestamp' => $dateoffeed]),
      '#metals_api_currency' => $this->t('Currency @currency', ['@currency' => $currency]),
      '#metals_api_unit' => $this->t('Rate @unit', ['@unit' => $unit]),
      '#metals_api_rates' => $rates_array,
      '#metals_api_options' => $options_array,
      '#attached' => [
        'library' => [
          'metals_api/metals_api_block',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $configFactory = $this->configFactory->get('metals_api.settings');
    $metals_api_access_key = $configFactory->get('metals_api_access_key');
    if (empty($metals_api_access_key)) {
      return AccessResult::forbidden();
    }
    else {
      return AccessResult::allowed();
    }
  }

}
