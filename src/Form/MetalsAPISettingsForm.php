<?php

namespace Drupal\metals_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class MetalsAPISettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'metals_api.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'metals_api_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['metals_api_access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Metals API Access Key'),
      '#description' => $this->t('<p>Enter your Metals API Access Key. If you don\'t have a Metals API Access Key, you can get one from <a href="@linkurl" target="_blank">@linktext</a></p>', ["@linkurl" => "https://metals-api.com/", "@linktext" => "metals-api.com"]),
      '#default_value' => $config->get('metals_api_access_key'),
    ];

    if ($config->get('metals_api_refresh_rate') < 1) {
      $metals_api_refresh_rate = 24;
    }
    else {
      $metals_api_refresh_rate = $config->get('metals_api_refresh_rate');
    }

    $form['metals_api_refresh_rate'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 24,
      '#step' => 1,
      '#title' => $this->t('Refresh Rate'),
      '#description' => $this->t("<p>Use the select field to set the rate in hours of how often the Metals API dataset should be refreshed. <br/>The default setting is to refresh once every 24 hours which will work with the Free API key. <br/>For a more timely rate, you will need to purchase an account that allows for more requests per month.</p>"),
      '#default_value' => $metals_api_refresh_rate,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('metals_api_access_key', $form_state->getValue('metals_api_access_key'))
      ->set('metals_api_refresh_rate', $form_state->getValue('metals_api_refresh_rate'))
      ->save();

    parent::submitForm($form, $form_state);
    drupal_flush_all_caches();
  }

}
