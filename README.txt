CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The Metals API module is a module that provides blocks of trading information
for precious metals acquired from the Metals API website at:
https://metals-api.com/.

A license from Metals API is necessary in order to use
this module. The FREE license allows for 50 requests per month. Providing
that the refresh rate for requests is set to 24 hours, it should allow for
enough requests to satisfy daily updates. If you would like to have more timely
updates then a paid license will be necessary.

Information about licenses from Metals API can be found here:
https://metals-api.com/pricing

REQUIREMENTS
------------
This module requires the Drupal's Internal Dynamic Page Cache to be active. If
It is disabled, Drupal will ask you if it should be enabled during the
installation of this module.

INSTALLATION
------------

* Without Composer *

  With DRUSH: drush en metals_api
  -or-
  Download it from https://www.drupal.org/project/metals_api and
  install it to your website.

* With Composer *
Install with Composer: $ composer require 'drupal/metals_api'

CONFIGURATION
------------
Before you can add a Metals API Block to your site you will need to configure
the Metals API module with an API Access Key and a Refresh Rate for caching the
Metals API dataset. Without an Access Key the Metals API Block will not render.

The preset value for the Refresh Rate is 24 Hours and that is suitable for use
with the FREE Metals API Metals API Access Key.

In order to increase the number of refreshes to the Metals API dataset, you will
need to acquire a paid license and API Access Key. More information can be found
on the Metal API Website: https://metals-api.com/pricing

MAINTAINERS
------------
The Metals API Module is Maintained  by Andrew Wasson:
https://www.drupal.org/u/awasson
